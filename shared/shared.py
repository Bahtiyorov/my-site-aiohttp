import bcrypt
import uuid

def generate_password_hash(password):
    password_bin= password.encode('utf-8')
    hashed = bcrypt.hashpw(password_bin, bcrypt.gensalt())
    return hashed.decode('utf-8')

def gen_uuid(namespace:str):
    return uuid.uuid5(uuid.NAMESPACE_DNS,namespace)