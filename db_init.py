#Иницализация база данных - init_db

from sqlalchemy import create_engine, MetaData

from  components.db import users, products, category

from components.settings import BASE_DIR, config



DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"

# psql -U postgres -h localhost
# CREATE DATABASE aiohttp_database;
# CREATE USER aiohttp_user WITH PASSWORD '1996';
# GRANT ALL PRIVILEGES ON DATABASE aiohttp_database TO aiohttp_user;
#or use
# def setup_db(config):
#     db_name=config['database']
#     db_user=config['user']
#     db_pass =config['password']
#
#     conn =admin_engine.connect()
#     conn.execute("DROP DATABASE IF EXISTS %s" % db_name)
#     conn.execute("DROP ROLE IF EXISTS %s" % db_user)
#     conn.execute("CREATE USER %s WITH PASSWORD '%s'" % (db_user, db_pass))
#     conn.execute("CREATE DATABASE %s ENCODING 'UTF8'" % db_name)
#     conn.execute("GRANT ALL PRIVILEGES ON DATABASE %s TO %s" % (db_name, db_user))
#     conn.close()


#create tables
def create_tables(engine):
    meta = MetaData()
    meta.create_all(bind=engine, tables=[users, category, products])


#insert data to tables
def sample_data(engine):
    conn=engine.connect()
    conn.execute(category.insert(),[
        {'name': 'samsung'},
        {'name':'iphone'},
        {'name':'nokia'},
        {'name':'huawey'}
    ])

    conn.execute(products.insert(),[
        {'category_id':1,'title':'Galaxy 6','img':'back1.jpg','description':'Its good phone'},
        {'category_id': 2, 'title': 'Galaxy 6', 'img': 'back1.jpg', 'description': 'Its good phone qqqq'},
        {'category_id': 3, 'title': 'iphone 6', 'img': 'back1.jpg', 'description': 'Its good phone eeee'},
        {'category_id': 4, 'title': 'nokia 1110', 'img': 'back1.jpg', 'description': 'Its good phone rrrrr'},
        {'category_id': 1, 'title': 'Galaxy 9', 'img': 'back1.jpg', 'description': 'Its good phone tttt'},
        {'category_id': 2, 'title': 'Iphone x', 'img': 'back1.jpg', 'description': 'Its good phone ttttt'},

    ])
    conn.close()


if __name__== '__main__':
    db_url=DSN.format(**config['postgres'])
    engine= create_engine(db_url)
    #setup_db(USER_CONFIG['postgres'])
    create_tables(engine)
    sample_data(engine)
