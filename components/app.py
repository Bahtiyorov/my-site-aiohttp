from aiohttp import web
from .routes import setup_routes
from components.db import init_db, close_db,  DBAuthorizationPolicy, setup_redis
import asyncio
from .middlewares import setup_middlewares
import jinja2
import aiohttp_jinja2


from aiohttp_security import SessionIdentityPolicy
from aiohttp_security import authorized_userid
from aiohttp_security import setup as setup_security
from aiohttp_session import setup as setup_session
from aiohttp_session.redis_storage import RedisStorage




async def current_user_ctx_processor(request):
    username = await authorized_userid(request)
    #print(username)
    is_anonymous = not bool(username)
    return {'current_user': {'is_anonymous': is_anonymous}}





async def create_app(config: dict):
    app=web.Application()
    app['config']=config
    #print(config['postgres']['database'])

    #setup views and routes
    setup_routes(app)
    setup_middlewares(app)

    # create db connection on startup, shutdown on exit
    db_pool=await init_db(app)
    app.on_startup.append(init_db)
    app.on_cleanup.append(close_db)

    redis_pool= await setup_redis(app)

    setup_session(app, RedisStorage(redis_pool))

    # setup jinja2 template render
    aiohttp_jinja2.setup(
        app,
        loader=jinja2.PackageLoader('components', 'templates'),
        context_processors=[current_user_ctx_processor]
    )

    setup_security(app,SessionIdentityPolicy(),DBAuthorizationPolicy(db_pool))

    config=app['config']['redis']
    #print(config['redis_host'])


    return app


