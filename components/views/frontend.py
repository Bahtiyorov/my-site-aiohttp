
from aiohttp import web
from aiohttp_jinja2 import template

from aiohttp_security import remember, forget, authorized_userid, check_authorized
from components.db import registration, check_user
from shared import shared

def redirect(router, router_name):
    location = router[router_name].url_for()
    return web.HTTPFound(location)


#login page
@template('login.html')
async def login(request):
    if request.method == 'POST':
        form_data=await request.post()
        email=form_data['email']
        password=form_data['password']
        #print(email,'\n',password)
        async with request.app['db'].acquire() as conn:
            check= await check_user(conn, email,password)
            if check:
                #print(check)
                response=redirect(request.app.router, 'index')
                email=check['email']

                uuid = shared.gen_uuid(namespace=email)
                uuid=str(uuid)
                #print(uuid)
                await remember(request,response,email,uuid=uuid)

                raise response
            else:
                print('такого пользователь не существует')

    return {}


    #return aiohttp.web.Response(text='ok')



async def logout(request):
    response = redirect(request.app.router, 'login')
    await forget(request, response)
    return response



#registration page
@template('register.html')
async def register(request):
    if request.method == 'POST':
        form_data= await request.post()
        #print(form_data['r_name'], form_data['email'], form_data['password'])
        password=form_data['password']

        #print(form_data)

        async with request.app['db'].acquire() as conn:
            result= await check_user(conn, form_data['email'])
            if result:
                error='User alredy exists'
                return {'error':error}
            if not result:
                await registration(conn, form_data['r_name'], form_data['email'], password)
                raise redirect(request.app.router, 'login')


    return {}

#home page
@template('home.html')
async def home(request):
    #uuid='dd14f98c-d342-5cf4-8e49-d2280fa1ec33'
    email, uuid=await authorized_userid(request)
    if not email:
        print('net')
          #raise redirect(request.app.router, 'login')


    #print(uuid)
    # async with request.app['db'].acquire() as conn:
    #     query =text("Select * from post;")
    #     result = await conn.fetch(query)
    # return aiohttp.web.Response(body=str(result))
    return web.Response(text="your email is {}".format(email))



@template('product.html')
async def product(request):
    email = await check_authorized(request)
    if not email:
        raise redirect(request.app.router, 'login')
    return {}

@template('about_us.html')
async def about(request):
    email = await check_authorized(request)
    if not email:
        raise redirect(request.app.router, 'login')
    return{}

@template('contacts.html')
async def contacts(request):
    email = await check_authorized(request)
    if not email:
        raise redirect(request.app.router, 'login')
    return{}

@template('account.html')
async def account(request):
    email = await check_authorized(request)
    if not email:
        raise redirect(request.app.router, 'login')
    return {}


