# Структура база данных
import aiopg.sa
import asyncpgsa
import aioredis

from sqlalchemy import (
MetaData, Table, Column, ForeignKey,
Integer, String, VARCHAR, create_engine
)


#__all__ =['users', 'category', 'products']


meta= MetaData()

#Структура таблиц

#таблица users
users = Table(
    'users', meta,
    Column('id', Integer, primary_key=True),
    Column('user_name', VARCHAR(200), nullable=False),
    Column('email', VARCHAR(200), nullable=False),
    Column('password', VARCHAR(200), nullable=False)
)

#таблица category
category=Table(
    'category', meta,
    Column('id', Integer, primary_key=True),
    Column('name', VARCHAR(200), nullable=False)
)
#таблица родуктс
products = Table(
    'products', meta,
    Column('id', Integer, primary_key=True),
    Column('category_id', Integer,
           ForeignKey('category.id', ondelete='CASCADE')
           ),
    Column('title', VARCHAR(200), nullable=True),
    Column('img', VARCHAR(200), nullable=True),
    Column('description', String(1000),nullable=True)
)


#Исключение ошибок
class RecordNotfound(Exception):
    """"Requested record in database was not found"""

#Подключение к бд
def constructor_db_url(conf):
        DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"
        return DSN.format(
        database=conf['database'],
        user= conf['user'],
        password=conf['password'],
        host =conf['host'],
        port=conf['port'],
        minsize=conf['minsize'],
        maxsize=conf['maxsize']
        )


async def init_db(app):
    dsn=constructor_db_url(app['config']['postgres'])
    #print(dsn)
    try:
        pool= await asyncpgsa.create_pool(dsn=dsn)
        app['db']=pool
        return pool
    except Exception as e:
        print(e)

#Отключение
async def close_db(app):
    print('close db')
    app['db'].close()
    app['db'].wait_closed()



# async def setup_redis(app):
#     async def init_redis(app):
#         try:
#             pool = await aioredis.create_pool((
#                 app['config']['redis']['redis_host'],
#                 app['config']['redis']['redis_port']
#             ))
#             print('connected to aioredis')
#             app['redis_pool'] = pool
#             return pool
#         except Exception as e:
#             print(e)
#
#     async def close_redis(app):
#         app['redis_pool'].close()
#         await app['redis_pool'].wait_closed()
#
#     #app.on_startup.append(init_redis)
#     app.on_cleanup.append(close_redis)
#     return  await init_redis(app)
async def setup_redis(app):

    pool = await aioredis.create_redis_pool((
        'localhost', 6379
    ))

    async def close_redis(app):
        print('closed pool')
        pool.close()
        await pool.wait_closed()

    app.on_cleanup.append(close_redis)
    app['redis_pool'] = pool
    return pool


async def registration(conn, username, email,password):
    stmt= users.insert().values(user_name=username, email=email, password=password)
    await conn.execute(stmt)


async def check_identity(conn, email):
    result= await conn.fetchrow(users.select().where(users.c.email==email))
    if result:
        return True
    return False

async def check_user(conn, email, password):
    result= await conn.fetchrow(users.select().where((users.c.email == email)&(users.c.password==password)))
    if result:
        records=dict(result)
        return records
    else:
        return None
