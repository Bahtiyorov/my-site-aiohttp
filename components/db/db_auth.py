from aiohttp_security.abc import AbstractAuthorizationPolicy

from components.db import check_identity

class DBAuthorizationPolicy(AbstractAuthorizationPolicy):
    def __init__(self, db_pool):
        self.db_pool=db_pool

    async def authorized_userid(self, identity, uuid=None):
        print(identity)
        async with self.db_pool.acquire() as conn:
            user=await check_identity(conn,identity)
            if user:
                #print(user)
                return identity, uuid
        return None

    async def permits(self, identity, permission, context=None):
        if identity is None:
            return False
        return True