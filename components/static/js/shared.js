/* Дополнительный методы */

    /* регулярные выжении */
    var regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

/* функция для перенаправление */
function reUrl(url)
{
    window.location=url;
}


 /* Методы для валидация поле */

    function validEmail(autoFocus,email,email_error) {
       var email_value=email.value;
        var errCount = 0;
        if (email_value === "" || !email_value.match(regex_email)) {
            email_error.innerHTML = "Не коректный e-mail или пустой";
            email.style.border = "1px solid red";
            console.log('hello');
            errCount++;
        }
        else {
            email_error.innerHTML = "";
            email.style.border = "1px solid #ccc";
        }

        if (errCount && autoFocus) email.focus();
        return errCount;
    }


    function validPassword(autoFocus, password, pass_error) {
        var pass_value = password.value;

        var errCount = 0;

        if (pass_value === "" || pass_value.length < 5) {
            errCount++;
            pass_error.innerHTML = "Введите пароль";
            password.style.border = "1px solid red";
        }
        else {
            pass_error.innerHTML = "";
            password.style.border = "1px solid #ccc";
        }
        if (errCount && autoFocus) password.focus();
        return errCount;
    }

       function validName(autoFocus, r_name, r_e_name) {
        var errCount = 0;
        var r_name_value=r_name.value;
        if (r_name_value === "" || r_name_value.length < 5) {
            errCount++;
            r_e_name.innerHTML = "Введите имя";
            r_name.style.border = "1px solid red";
        }
        else {
            r_e_name.innerHTML = "";
            r_name.style.border = "1px solid #ccc";
        }
        if (errCount && autoFocus) r_name.focus();
        return errCount;

    }

    function validConfirmPassword(autoFocus,r_password, r_c_password, r_e_c_password) {
        var errCount = 0;
        var r_password_value = r_password.value;

        if (r_c_password.value != r_password_value) {
            errCount++;
            r_e_c_password.innerHTML = "Повторите пароль верно";
            r_c_password.style.border = "1px solid red";
        }
        else if (r_c_password.value === "") {
            errCount++;
            r_e_c_password.innerHTML = "Введите повторение пароль";
            r_c_password.style.border = "1px solid red";
        }

        else {
            r_e_c_password.innerHTML = "";
            r_c_password.style.border = "1px solid #ccc";
        }
        if (errCount && autoFocus) r_c_password.focus();
        return errCount;
    }


