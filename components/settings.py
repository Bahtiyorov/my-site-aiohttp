from pathlib import Path
import yaml

BASE_DIR =Path(__file__).parent/ 'config.yaml'
print(BASE_DIR)

#__all__=('config', 'load_config')


def load_config(config_file=None):
    default_path=Path(__file__).parent/ 'config.yaml'
    with open(default_path, 'r') as f:
        config= yaml.safe_load(f)

    cf_dict={}

    if config_file:
        cf_dict=yaml.safe_load(config_file)

    config.update(**cf_dict)
    #print(str(BASE_DIR))
    return config

#config = load_config(BASE_DIR)

def get_config(path):
    with open(path) as f:
        config = yaml.load(f)
    return config

config = get_config(BASE_DIR)