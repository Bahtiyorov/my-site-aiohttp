import pathlib

from .views import frontend
PROJECT_ROOT=pathlib.Path(__file__).parent

#Роути проекта
def setup_routes(app):
    app.router.add_route('*','/', frontend.login, name='login')
    #app.router.add_route('POST', '/', frontend.login)
    app.router.add_get(r'/registration', frontend.register, name='registration')
    app.router.add_post(r'/registration', frontend.register)

    app.router.add_route('GET', r'/home', frontend.home, name='index')
    app.router.add_route('GET', r'/products', frontend.product)
    app.router.add_route('GET', r'/about', frontend.about)
    app.router.add_route('GET', r'/contacts', frontend.contacts)
    app.router.add_route('GET', r'/account', frontend.account)
    app.router.add_get('/logout', frontend.logout, name='logout')



    setup_static_routes(app)


#Роути статического файлы
def setup_static_routes(app):
    app.router.add_static('/static/',
                          path=PROJECT_ROOT/'static',
                          name='static',
                          follow_symlinks=True
                          )


